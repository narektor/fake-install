// this script runs before everything else in the page
// and patches window.* apis to mimic an installed pwa

// patch window.navigator
window.navigator.standalone = true;
Navigator.prototype.standalone = true;
// patch window.matchMedia
var __base_window_matchMedia = window.matchMedia;
window.matchMedia = function(arg) {
	if (arg.includes("standalone")) {
		return { media: arg, matches: true, onchange: null }
	} else {
		return __base_window_matchMedia(arg);
	}
}

console.log("patched APIs");