/*
actualCode contains a base64 encoded version of the script
that simulates an installed app. The rest of this code
decodes it and puts it into a script tag.

Note for contributors: do not edit the text in "code", edit
script.js and run build.py instead. Also, do not remove any
comments that start with "!{" and end with "}!".
*/

var code =
//!{bst}!
"Ly8gdGhpcyBzY3JpcHQgcnVucyBiZWZvcmUgZXZlcnl0aGluZyBlbHNlIGluIHRoZSBwYWdlCi"+
"8vIGFuZCBwYXRjaGVzIHdpbmRvdy4qIGFwaXMgdG8gbWltaWMgYW4gaW5zdGFsbGVkIHB3YQoK"+
"Ly8gcGF0Y2ggd2luZG93Lm5hdmlnYXRvcgp3aW5kb3cubmF2aWdhdG9yLnN0YW5kYWxvbmUgPS"+
"B0cnVlOwpOYXZpZ2F0b3IucHJvdG90eXBlLnN0YW5kYWxvbmUgPSB0cnVlOwovLyBwYXRjaCB3"+
"aW5kb3cubWF0Y2hNZWRpYQpfX2Jhc2Vfd2luZG93X21hdGNoTWVkaWEgPSB3aW5kb3cubWF0Y2"+
"hNZWRpYTsKd2luZG93Lm1hdGNoTWVkaWEgPSBmdW5jdGlvbihhcmcpIHsKCWlmIChhcmcuaW5j"+
"bHVkZXMoInN0YW5kYWxvbmUiKSkgewoJCXJldHVybiB7IG1lZGlhOiBhcmcsIG1hdGNoZXM6IH"+
"RydWUsIG9uY2hhbmdlOiBudWxsIH0KCX0gZWxzZSB7CgkJcmV0dXJuIF9fYmFzZV93aW5kb3df"+
"bWF0Y2hNZWRpYShhcmcpOwoJfQp9Cgpjb25zb2xlLmxvZygicGF0Y2hlZCBBUElzIik7";
//!{ben}!

var s = document.createElement('script');
s.textContent = atob(code);
(document.head||document.documentElement).appendChild(s);
s.remove();
console.log("added script");