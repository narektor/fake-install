const canInstall = 'standalone' in window.navigator;
const isInstalled = window.navigator.standalone === true;
const isSafari = navigator.vendor && navigator.vendor.indexOf('Apple') > -1 &&
               navigator.userAgent &&
               navigator.userAgent.indexOf('CriOS') == -1 &&
               navigator.userAgent.indexOf('FxiOS') == -1;

const notMobileElement = document.getElementById("not-mobile");
const cannotInstallElement = document.getElementById("cannot-install");
const notInstalledElement = document.getElementById("not-installed");
const webAppElement = document.getElementById("installed");

if (isSafari) {
	console.log("detecting using window.navigator method.");
	console.log(window.navigator);
	console.log(`canInstall: ${canInstall}`);
	console.log(`isInstalled: ${isInstalled}`);

	if (!isInstalled) notInstalledElement.classList.remove("hide");
	else webAppElement.classList.remove("hide");
} else {
	console.log("not safari!");
	notMobileElement.classList.remove("hide");
}