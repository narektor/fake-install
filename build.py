import re
from base64 import b64encode
from zipfile import ZipFile
import os

BLOB_RE = re.compile(r"!{bst}!.+!{ben}!", re.DOTALL)
MAX_LINE_LEN = 70

def splitToLines(string):
	# get the amount of lines
	l = len(string) / MAX_LINE_LEN
	lr = round(l)
	# if it doesn't divide cleanly, add one to lr
	# turns out python is forgiving and will allow reading beyond the length of the string
	if lr < l:
		lr += 1
	# split it to lines with equal-ish chars
	lines = []
	for i in range(lr):
		lines.append(string[i*MAX_LINE_LEN:(i+1)*MAX_LINE_LEN])
	return lines

def stringToJS(string):
	lines = splitToLines(string)
	# join the lines into something js looking
	join = '" +\n"'.join(lines)
	return '"' + join + '";'

def cleanJS(string): 
	nstr = string
	nstr = nstr.replace("	","")
	# remove comments
	nstr = re.sub(r"//.*", "", nstr)
	# spaces
	nstr = nstr.replace(" = ","=")
	nstr = nstr.replace(": ",":")
	nstr = nstr.replace(") {","){")
	nstr = nstr.replace("if (", "if(")
	nstr = nstr.replace("else {", "else{")
	nstr = nstr.replace("} else", "}else")
	# newlines
	nstr = nstr.replace("\n","")
	return nstr

def packExtension(zip):
	# add manifest
	zip.write('extension/manifest.json', "manifest.json")
	# add script
	zip.write('extension/injectScript.js', "injectScript.js")
	# add logos
	zip.write('extension/logo/32.png', "logo/32.png")
	zip.write('extension/logo/64.png', "logo/64.png")
	zip.write('extension/logo/256.png', "logo/256.png")

print("Encoding script...");
lines = ""
with open("extension/script.js", "r") as f:
	lines = b64encode(cleanJS(f.read()).encode('utf-8'))
	lines = lines.decode('utf-8')

print("Replacing base64 blob...")
new_blob = "!{bst}!\n" + stringToJS(lines) + "\n//!{ben}!"
data = ""
with open("extension/injectScript.js", "r") as f:
	data = f.read()
data = re.sub(BLOB_RE, new_blob, data)
with open("extension/injectScript.js", "w") as f:
	f.write(data)

if not os.path.exists("build"):
	os.mkdir("build")

print("Packing crx...")
with ZipFile('build/FakeInstall.crx', 'w') as crx:
 	packExtension(crx)

print("Packing xpi...")
with ZipFile('build/FakeInstall.xpi', 'w') as xpi:
 	packExtension(xpi)
